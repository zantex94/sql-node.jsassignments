"use strict";
//15.8.4. Assignment Node II.2
//15.8.5. Assignment Node II.3
//node ./myg54/main.js
// file:///Users/reneseebach/Visual-Studio_Projects/sql-node.jsassignments/myg54/indexget.html
// result: Log: Request Body Contents: 
// {
//     "GET": {
//         "name": "René Seebach",
//         "password": "12342312",
//         "email": "rene392i@iba.dk",
//         "phoneNumber": "24312431"
//     },
//     "POST": {}
// }
// file:///Users/reneseebach/Visual-Studio_Projects/sql-node.jsassignments/myg54/indexpost.html
// result: Log: Request Body Contents: name=Ren%C3%A9+Seebach&password=345756324&email=reneseer%40gmail.com&phoneNumber=2413214
// {
//     "GET": {},
//     "POST": {
//         "name": "René Seebach",
//         "password": "345756324",
//         "email": "reneseer@gmail.com",
//         "phoneNumber": "2413214"
//     }
// }
const routeMap = {
    "/info": "<h1>Info Page</h1>",
    "/contact": "<h1>Contact Us at:</h1>",
    "/about": "<h1>Learn About Us</h1>",
    "/hello": "<h1>Send Us an Email </h1>",
    "/error": "<h1>The page you wanted doesn't exist</h1>",
    "/": "<h1>Welcome</h1>"
};

const http = require("http");
const httpStatus = require("http-status-codes");
const lib = require("./libWebUtil");
const hostname = "127.0.0.1";
const port = 3000;
const app = http.createServer();            // server as an obj


app.on("request", function (req, res) {      // eventhandler for "request"
    console.log(lib.makeLogEntry(req));     
    let body = [];
    req.on("data", function (bodyData) {    // eventhandling for data reception
        console.log(lib.makeWebArrays(req,bodyData));
        body.push(bodyData);                // bodyData is an object
    });
    req.on("end", function () {             // eventhandling for end-of-data
        body = Buffer.concat(body).toString();
        console.log("Log: Request Body Contents: " + body);
        console.log("test makearrays : " + lib.makeWebArrays(req,body));

    });

    console.log("Log: Method: " + req.method);
    console.log("Log: URL: " + lib.getJSONString(req.url));
    console.log("Log: Headers:\n" + lib.getJSONString(req.headers));
                                            // prep response header
    res.writeHead(httpStatus.OK, {
        "Content-Type": "text/html; charset=utf-8"
    });        

    let responseMsg;
    if (routeMap[req.url]) {                // look for route
     responseMsg = routeMap[req.url];    // found, use it
     } else {
    responseMsg = "<h1>This is my 404, reguested url not found</h1>";   // else show something else
    }
    responseMsg += "<p><kbd>myg54</kbd> is helping him</p>";
    res.write(responseMsg);                 // respond
    res.end();                              // sends response http

});

app.listen(port, hostname, function () {
    console.log(`Server running, and listening at http://${hostname}:${port}/`);
});
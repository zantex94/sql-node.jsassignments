"use strict";
//15.8.2. Assignment Node II.1
//using my own local path
// file:///Users/reneseebach/Visual-Studio_Projects/sql-node.jsassignments/myg53/indexget.html
// result: Log: URL: "/?name=Ren%C3%A9+Seebach&password=1234&email=reneseer%40gmail.com&phoneNumber=20913871"
// 15.8.3. Assignment Node II.1a
// file:///Users/reneseebach/Visual-Studio_Projects/sql-node.jsassignments/myg53/indexpost.html
// result: Log: Request Body Contents: name=Ren%C3%A9+Seebach&password=1234&email=rene392i%40iba.dk&phoneNumber=31421342
//node ./myg53/main.js
// You can use both GET AND POST to collect data. Both use a request to the server for collecting data and GET is not the only way to get that.
const routeMap = {
    "/info": "<h1>Info Page</h1>",
    "/contact": "<h1>Contact Us at:</h1>",
    "/about": "<h1>Learn About Us</h1>",
    "/hello": "<h1>Send Us an Email </h1>",
    "/error": "<h1>The page you wanted doesn't exist</h1>"
};

const http = require("http");
const httpStatus = require("http-status-codes");
const hostname = "127.0.0.1";
const port = 3000;
const app = http.createServer();            // server as an obj

const getJSONString = function (obj) {      // prettyprint obj
    return JSON.stringify(obj, null, 4);
}

app.on("request", function (req, res) {     // eventhandler for "request"
    let body = [];
    req.on("data", function (bodyData) {    // eventhandling for data reception
        body.push(bodyData);                // bodyData is an object
    });
    req.on("end", function () {             // eventhandling for end-of-data
        body = Buffer.concat(body).toString();
        console.log("Log: Request Body Contents: " + body);
    });

    console.log("Log: Method: " + req.method);
    console.log("Log: URL: " + getJSONString(req.url));
    console.log("Log: Headers:\n" + getJSONString(req.headers));
                                            // prep response header
    res.writeHead(httpStatus.OK, {
        "Content-Type": "text/html; charset=utf-8"
    });        

    let responseMsg;
    if (routeMap[req.url]) {                // look for route
     responseMsg = routeMap[req.url];    // found, use it
     } else {
    responseMsg = "<h1>Kilroy Was Here Too</h1>";   // else show something else
    }
    responseMsg += "<p><kbd>myg53</kbd> is helping him</p>";
    res.write(responseMsg);                 // respond
    res.end();                              // sends response http

});

app.listen(port, hostname, function () {
    console.log(`Server running, and listening at http://${hostname}:${port}/`);
});
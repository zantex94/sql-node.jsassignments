use worldTest;


set @spørgsmål := 1;
select @spørgsmål;

select * from city where countryCode="AFG";

set @spørgsmål := 2;
select @spørgsmål;

select * from city where countrycode in ("TJK");

set @spørgsmål := 3;
select @spørgsmål;

select * from city where countryCode="UZB";

set @spørgsmål := 4;
select @spørgsmål;

 select * from city where countryCode in ("UZB","TJK","AFG");

set @spørgsmål := 5;
select @spørgsmål;
@svar:=‘Population is missing to identify how many people that speaks a language.’;

set @spørgsmål := 6;
select @spørgsmål;

 select * from countrylanguage where language="Danish";

set @spørgsmål := 7;
select @spørgsmål;

select * from countrylanguage where countrycode="ZAF" order by language;


set @spørgsmål := 8;
select @spørgsmål;

select * from countrylanguage where countrycode in ("UZB","TJK","AFG") order by percentage desc;

set @spørgsmål := 9;
select @spørgsmål;

select headofstate from country where continent="Europe";


set @spørgsmål := 10;
select @spørgsmål;

select code, name, population  from country where region="Southern and Central Asia" order by population;

set @spørgsmål := 11;
select @spørgsmål;

select name, population, continent, capital from country where continent in ("Oceania","Antarctica","South America");

set @spørgsmål := 12;
select @spørgsmål;

select name, population, language, percentage, population / 100 * percentage as 'spoken' from country join countrylanguage on code=countrycode group by language;

set @spørgsmål := 13;
select @spørgsmål;

select count(language) as 'different language in the world' from countrylanguage;

set @spørgsmål := 14;
select @spørgsmål;

select  a.name, a.countrycode from city a join (select name, countrycode, count(*) from city group by name having count(*) > 1 ) b on a.name = b.name order by a.name;




"use strict";

exports.checkvalues = function(x){
    console.log(x);
    if(x === null || x === "" || x === undefined){
        return false;
    }else{
        return true;
    }


}

let fs = require('fs');
let filename = process.argv[2];

fs.readFile(filename, function (err, data) {
    if (err) {
        throw err;
    }
    console.log(`${filename} content:\n------------------------\n ${data}`);
});
console.log("Asynchronously? If you see me first it was.");


let content = `Hello world. Lorem ipsum dolor sit amet, consectetur adipiscing elit,
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
`;

fs.writeFile(filename, content , (err) => {
    if (err) {
        throw err;
    }
    console.log("yes! It's written asynchronously.");
});
console.log("Asynchronously?");